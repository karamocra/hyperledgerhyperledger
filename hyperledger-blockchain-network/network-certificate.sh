source scriptUtils.sh
export PATH=${PWD}/bin:$PATH

certificatesForHospital() {
  echo
  echo "Enrolling the CA admin"
  mkdir -p consortium/crypto-config/peerOrganizations/hospital/
  export FABRIC_CA_CLIENT_HOME=${PWD}/consortium/crypto-config/peerOrganizations/hospital/

   
  fabric-ca-client enroll -u https://admin:adminpw@localhost:1010 --caname ca_hospital --tls.certfiles ${PWD}/consortium/crypto-config/peerOrganizations/hospital/tls-cert.pem
 

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-1010-ca-hospital.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-1010-ca-hospital.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-1010-ca-hospital.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-1010-ca-hospital.pem
    OrganizationalUnitIdentifier: orderer' > "${PWD}/consortium/crypto-config/peerOrganizations/hospital/msp/config.yaml"
  echo
  echo "Registering peer0"
  
  fabric-ca-client register --caname ca-hospital --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles "${PWD}/consortium/fabric-ca/hospital/tls-cert.pem"
 
  echo
  echo "Registering peer1"

  fabric-ca-client register --caname ca-hospital --id.name peer1 --id.secret peer0pw --id.type peer --tls.certfiles "${PWD}/consortium/fabric-ca/hospital/tls-cert.pem"
  

  echo
  echo "Registering user"
  echo
  fabric-ca-client register --caname ca-hospital --id.name user1 --id.secret user1pw --id.type client --tls.certfiles "${PWD}/consortium/fabric-ca/hospital/tls-cert.pem"
  
  echo
  echo "Registering the org admin"
  echo
  fabric-ca-client register --caname ca-hospital --id.name hospitaladmin --id.secret org1adminpw --id.type admin --tls.certfiles "${PWD}/consortium/fabric-ca/hospital/tls-cert.pem"
  
   mkdir -p /consortium/crypto-config/peerOrganizations/hospital/peers


  mkdir -p /consortium/crypto-config/peerOrganizations/hospital/peers/peer0.hospital 
  echo  "Generating the peer0 msp"
  echo
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:1010 --caname ca-hospital -M "${PWD}/consortium/crypto-config/peerOrganizations/hospital/peers/peer0.hospital/msp" --csr.hosts peer0.hospital --tls.certfiles "${PWD}/consortium/fabric-ca/hospital/tls-cert.pem"


  cp "${PWD}/consortium/crypto-config/peerOrganizations/hospital/msp/config.yaml" "${PWD}/consortium/crypto-config/peerOrganizations/hospital/peers/peer0.hospital/msp/config.yaml"

  echo
  echo "Generating the peer0-tls certificates"
  echo
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:1010 --caname ca-hospital -M "${PWD}/consortium/crypto-config/peerOrganizations/hospital/peers/peer0.hospital/tls" --enrollment.profile tls --csr.hosts peer0.hospital --csr.hosts localhost --tls.certfiles "${PWD}/consortium/fabric-ca/hospital/tls-cert.pem"
  

  cp "${PWD}/consortium/crypto-config/peerOrganizations/hospital/peers/peer0.hospital/tls/tlscacerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/hospital/peers/peer0.hospital/tls/ca.crt"
  cp "${PWD}/consortium/crypto-config/peerOrganizations/hospital/peers/peer0.hospital/tls/signcerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/hospital/peers/peer0.hospital/tls/server.crt"
  cp "${PWD}/consortium/crypto-config/peerOrganizations/hospital/peers/peer0.hospital/tls/keystore/"* "${PWD}/consortium/crypto-config/peerOrganizations/hospital/peers/peer0.hospital/tls/server.key"

  mkdir -p "${PWD}/consortium/crypto-config/peerOrganizations/hospital/msp/tlscacerts"
  cp "${PWD}/consortium/crypto-config/peerOrganizations/hospital/peers/peer0.hospital/tls/tlscacerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/hospital/msp/tlscacerts/ca.crt"

  mkdir -p "${PWD}/consortium/crypto-config/peerOrganizations/hospital/tlsca"
  cp "${PWD}/consortium/crypto-config/peerOrganizations/hospital/peers/peer0.hospital/tls/tlscacerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/hospital/peers/peer0.hospital/tlsca/tlsca.hospital-cert.pem"

  mkdir -p "${PWD}/consortium/crypto-config/peerOrganizations/hospital/ca"
  cp "${PWD}consortium/crypto-config/peerOrganizations/hospital/peers/peer0.hospital/msp/cacerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/hospital/ca/ca.hospital-cert.pem"

  infoln "Generating the user msp"
  set -x
  fabric-ca-client enroll -u https://user1:user1pw@localhost:1010 --caname ca-hospital -M "${PWD}/consortium/crypto-config/peerOrganizations/hospital/users/User1@hospital/msp" --tls.certfiles "${PWD}/consortium/fabric-ca/hospital/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/consortium/crypto-config/peerOrganizations/hospital/msp/config.yaml" "${PWD}/consortium/crypto-config/peerOrganizations/hospital/users/User1@hospital/msp/config.yaml"
  echo
  echo "Generating the org admin msp"
  echo
  fabric-ca-client enroll -u https://org1admin:org1adminpw@localhost:1010 --caname ca-hospital -M "${PWD}/consortium/crypto-config/peerOrganizations/hospital/users/Admin@hospital/msp" --tls.certfiles "${PWD}/consortium/fabric-ca/hospital/tls-cert.pem"
 

  cp "${PWD}/consortium/crypto-config/peerOrganizations/hospital/msp/config.yaml" "${PWD}/consortium/crypto-config/peerOrganizations/hospital/users/Admin@hospital/msp/config.yaml"
}


certificatesForLaboratory() {
  echo
  echo "Enrolling the CA admin"
  mkdir -p consortium/crypto-config/peerOrganizations/labaratory/
  export FABRIC_CA_CLIENT_HOME=${PWD}/consortium/crypto-config/peerOrganizations/labaratory/


  fabric-ca-client enroll -u https://admin:adminpw@localhost:1020 --caname ca_labaratory --tls.certfiles ${PWD}/consortium/crypto-config/peerOrganizations/labaratory/tls-cert.pem
  

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-1020-ca-labaratory.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-1020-ca-labaratory.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-1020-ca-labaratory.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-1020-ca-labaratory.pem
    OrganizationalUnitIdentifier: orderer' > "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/msp/config.yaml"
  echo
  echo "Registering peer0"
  echo 
  fabric-ca-client register --caname ca-labaratory --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles "${PWD}/consortium/fabric-ca/labaratory/tls-cert.pem"
  
  echo
  echo "Registering peer1"
  echo 
  fabric-ca-client register --caname ca-labaratory --id.name peer1 --id.secret peer0pw --id.type peer --tls.certfiles "${PWD}/consortium/fabric-ca/labaratory/tls-cert.pem"
  

  echo
  echo "Registering user"
  echo
  fabric-ca-client register --caname ca-labaratory --id.name user1 --id.secret user1pw --id.type client --tls.certfiles "${PWD}/consortium/fabric-ca/labaratory/tls-cert.pem"
  
  echo
  echo "Registering the org admin"
  echo
  fabric-ca-client register --caname ca-labaratory --id.name labaratoryadmin --id.secret org1adminpw --id.type admin --tls.certfiles "${PWD}/consortium/fabric-ca/labaratory/tls-cert.pem"
  
   mkdir -p /consortium/crypto-config/peerOrganizations/labaratory/peers


  mkdir -p /consortium/crypto-config/peerOrganizations/labaratory/peers/peer0.labaratory 
  echo  "Generating the peer0 msp"
  echo
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:1020 --caname ca-labaratory -M "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/peers/peer0.labaratory/msp" --csr.hosts peer0.labaratory --tls.certfiles "${PWD}/consortium/fabric-ca/labaratory/tls-cert.pem"


  cp "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/msp/config.yaml" "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/peers/peer0.labaratory/msp/config.yaml"

  echo
  echo "Generating the peer0-tls certificates"
  echo
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:1020 --caname ca-org1 -M "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/peers/peer0.labaratory/tls" --enrollment.profile tls --csr.hosts peer0.labaratory --csr.hosts localhost --tls.certfiles "${PWD}/consortium/fabric-ca/labaratory/tls-cert.pem"
  

  cp "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/peers/peer0.labaratory/tls/tlscacerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/peers/peer0.labaratory/tls/ca.crt"
  cp "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/peers/peer0.labaratory/tls/signcerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/peers/peer0.labaratory/tls/server.crt"
  cp "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/peers/peer0.labaratory/tls/keystore/"* "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/peers/peer0.labaratory/tls/server.key"

  mkdir -p "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/msp/tlscacerts"
  cp "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/peers/peer0.labaratory/tls/tlscacerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/msp/tlscacerts/ca.crt"

  mkdir -p "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/tlsca"
  cp "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/peers/peer0.labaratory/tls/tlscacerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/peers/peer0.labaratory/tlsca/tlsca.labaratory-cert.pem"

  mkdir -p "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/ca"
  cp "${PWD}consortium/crypto-config/peerOrganizations/labaratory/peers/peer0.labaratory/msp/cacerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/ca/ca.labaratory-cert.pem"

  infoln "Generating the user msp"
  set -x
  fabric-ca-client enroll -u https://user1:user1pw@localhost:1020 --caname ca-org1 -M "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/users/User1@labaratory/msp" --tls.certfiles "${PWD}/consortium/fabric-ca/labaratory/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/msp/config.yaml" "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/users/User1@labaratory/msp/config.yaml"
  echo
  echo "Generating the org admin msp"
  echo
  fabric-ca-client enroll -u https://org1admin:org1adminpw@localhost:1020 --caname ca-org1 -M "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/users/Admin@labaratory/msp" --tls.certfiles "${PWD}/consortium/fabric-ca/labaratory/tls-cert.pem"
 

  cp "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/msp/config.yaml" "${PWD}/consortium/crypto-config/peerOrganizations/labaratory/users/Admin@labaratory/msp/config.yaml"
}

certificatesForPharamacy() {
  echo
  echo "Enrolling the CA admin"
  mkdir -p consortium/crypto-config/peerOrganizations/pharamacy/
  export FABRIC_CA_CLIENT_HOME=${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/


  fabric-ca-client enroll -u https://admin:adminpw@localhost:1040 --caname ca_pharamacy --tls.certfiles ${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/tls-cert.pem
  

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-1040-ca-pharamacy.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-1040-ca-pharamacy.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-1040-ca-pharamacy.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-1040-ca-pharamacy.pem
    OrganizationalUnitIdentifier: orderer' > "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/msp/config.yaml"
  echo
  echo "Registering peer0"
  echo 
  fabric-ca-client register --caname ca-pharamacy --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles "${PWD}/consortium/fabric-ca/pharamacy/tls-cert.pem"
  
  echo
  echo "Registering peer1"
  echo 
  fabric-ca-client register --caname ca-pharamacy --id.name peer1 --id.secret peer0pw --id.type peer --tls.certfiles "${PWD}/consortium/fabric-ca/pharamacy/tls-cert.pem"
  

  echo
  echo "Registering user"
  echo
  fabric-ca-client register --caname ca-pharamacy --id.name user1 --id.secret user1pw --id.type client --tls.certfiles "${PWD}/consortium/fabric-ca/pharamacy/tls-cert.pem"
  
  echo
  echo "Registering the org admin"
  echo
  fabric-ca-client register --caname ca-pharamacy --id.name pharamacyadmin --id.secret org1adminpw --id.type admin --tls.certfiles "${PWD}/consortium/fabric-ca/pharamacy/tls-cert.pem"
  
   mkdir -p /consortium/crypto-config/peerOrganizations/pharamacy/peers


  mkdir -p /consortium/crypto-config/peerOrganizations/pharamacy/peers/peer0.pharamacy 
  echo  "Generating the peer0 msp"
  echo
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:1040 --caname ca-pharamacy -M "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/peers/peer0.pharamacy/msp" --csr.hosts peer0.pharamacy --tls.certfiles "${PWD}/consortium/fabric-ca/pharamacy/tls-cert.pem"


  cp "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/msp/config.yaml" "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/peers/peer0.pharamacy/msp/config.yaml"

  echo
  echo "Generating the peer0-tls certificates"
  echo
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:1040 --caname ca-org1 -M "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/peers/peer0.pharamacy/tls" --enrollment.profile tls --csr.hosts peer0.pharamacy --csr.hosts localhost --tls.certfiles "${PWD}/consortium/fabric-ca/pharamacy/tls-cert.pem"
  

  cp "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/peers/peer0.pharamacy/tls/tlscacerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/peers/peer0.pharamacy/tls/ca.crt"
  cp "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/peers/peer0.pharamacy/tls/signcerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/peers/peer0.pharamacy/tls/server.crt"
  cp "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/peers/peer0.pharamacy/tls/keystore/"* "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/peers/peer0.pharamacy/tls/server.key"

  mkdir -p "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/msp/tlscacerts"
  cp "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/peers/peer0.pharamacy/tls/tlscacerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/msp/tlscacerts/ca.crt"

  mkdir -p "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/tlsca"
  cp "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/peers/peer0.pharamacy/tls/tlscacerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/peers/peer0.pharamacy/tlsca/tlsca.pharamacy-cert.pem"

  mkdir -p "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/ca"
  cp "${PWD}consortium/crypto-config/peerOrganizations/pharamacy/peers/peer0.pharamacy/msp/cacerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/ca/ca.pharamacy-cert.pem"

  infoln "Generating the user msp"
  set -x
  fabric-ca-client enroll -u https://user1:user1pw@localhost:1040 --caname ca-org1 -M "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/users/User1@pharamacy/msp" --tls.certfiles "${PWD}/consortium/fabric-ca/pharamacy/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/msp/config.yaml" "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/users/User1@pharamacy/msp/config.yaml"
  echo
  echo "Generating the org admin msp"
  echo
  fabric-ca-client enroll -u https://org1admin:org1adminpw@localhost:1040 --caname ca-org1 -M "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/users/Admin@pharamacy/msp" --tls.certfiles "${PWD}/consortium/fabric-ca/pharamacy/tls-cert.pem"
 

  cp "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/msp/config.yaml" "${PWD}/consortium/crypto-config/peerOrganizations/pharamacy/users/Admin@pharamacy/msp/config.yaml"
}


certificatesForIns-provider() {
  echo
  echo "Enrolling the CA admin"
  mkdir -p consortium/crypto-config/peerOrganizations/ins-provider/
  export FABRIC_CA_CLIENT_HOME=${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/


  fabric-ca-client enroll -u https://admin:adminpw@localhost:1050 --caname ca_ins-provider --tls.certfiles ${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/tls-cert.pem
  

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-1050-ca-ins-provider.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-1050-ca-ins-provider.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-1050-ca-ins-provider.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-1050-ca-ins-provider.pem
    OrganizationalUnitIdentifier: orderer' > "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/msp/config.yaml"
  echo
  echo "Registering peer0"
  echo 
  fabric-ca-client register --caname ca-ins-provider --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles "${PWD}/consortium/fabric-ca/ins-provider/tls-cert.pem"
  
  echo
  echo "Registering peer1"
  echo 
  fabric-ca-client register --caname ca-ins-provider --id.name peer1 --id.secret peer0pw --id.type peer --tls.certfiles "${PWD}/consortium/fabric-ca/ins-provider/tls-cert.pem"
  

  echo
  echo "Registering user"
  echo
  fabric-ca-client register --caname ca-ins-provider --id.name user1 --id.secret user1pw --id.type client --tls.certfiles "${PWD}/consortium/fabric-ca/ins-provider/tls-cert.pem"
  
  echo
  echo "Registering the org admin"
  echo
  fabric-ca-client register --caname ca-ins-provider --id.name ins-provideradmin --id.secret org1adminpw --id.type admin --tls.certfiles "${PWD}/consortium/fabric-ca/ins-provider/tls-cert.pem"
  
   mkdir -p /consortium/crypto-config/peerOrganizations/ins-provider/peers


  mkdir -p /consortium/crypto-config/peerOrganizations/ins-provider/peers/peer0.ins-provider 
  echo  "Generating the peer0 msp"
  echo
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:1050 --caname ca-ins-provider -M "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/peers/peer0.ins-provider/msp" --csr.hosts peer0.ins-provider --tls.certfiles "${PWD}/consortium/fabric-ca/ins-provider/tls-cert.pem"


  cp "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/msp/config.yaml" "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/peers/peer0.ins-provider/msp/config.yaml"

  echo
  echo "Generating the peer0-tls certificates"
  echo
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:1050 --caname ca-org1 -M "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/peers/peer0.ins-provider/tls" --enrollment.profile tls --csr.hosts peer0.ins-provider --csr.hosts localhost --tls.certfiles "${PWD}/consortium/fabric-ca/ins-provider/tls-cert.pem"
  

  cp "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/peers/peer0.ins-provider/tls/tlscacerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/peers/peer0.ins-provider/tls/ca.crt"
  cp "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/peers/peer0.ins-provider/tls/signcerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/peers/peer0.ins-provider/tls/server.crt"
  cp "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/peers/peer0.ins-provider/tls/keystore/"* "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/peers/peer0.ins-provider/tls/server.key"

  mkdir -p "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/msp/tlscacerts"
  cp "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/peers/peer0.ins-provider/tls/tlscacerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/msp/tlscacerts/ca.crt"

  mkdir -p "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/tlsca"
  cp "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/peers/peer0.ins-provider/tls/tlscacerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/peers/peer0.ins-provider/tlsca/tlsca.ins-provider-cert.pem"

  mkdir -p "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/ca"
  cp "${PWD}consortium/crypto-config/peerOrganizations/ins-provider/peers/peer0.ins-provider/msp/cacerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/ca/ca.ins-provider-cert.pem"

  infoln "Generating the user msp"
  set -x
  fabric-ca-client enroll -u https://user1:user1pw@localhost:1050 --caname ca-org1 -M "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/users/User1@ins-provider/msp" --tls.certfiles "${PWD}/consortium/fabric-ca/ins-provider/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/msp/config.yaml" "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/users/User1@ins-provider/msp/config.yaml"
  echo
  echo "Generating the org admin msp"
  echo
  fabric-ca-client enroll -u https://org1admin:org1adminpw@localhost:1050 --caname ca-org1 -M "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/users/Admin@ins-provider/msp" --tls.certfiles "${PWD}/consortium/fabric-ca/ins-provider/tls-cert.pem"
 

  cp "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/msp/config.yaml" "${PWD}/consortium/crypto-config/peerOrganizations/ins-provider/users/Admin@ins-provider/msp/config.yaml"
}

certificatesForOrderer() {
  echo
  echo "Enrolling the CA admin"
  mkdir -p consortium/crypto-config/peerOrganizations/orderer/
  export FABRIC_CA_CLIENT_HOME=${PWD}/consortium/crypto-config/peerOrganizations/orderer/


  fabric-ca-client enroll -u https://admin:adminpw@localhost:1030 --caname ca_orderer --tls.certfiles ${PWD}/consortium/crypto-config/peerOrganizations/orderer/tls-cert.pem
  

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-1030-ca-orderer.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-1030-ca-orderer.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-1030-ca-orderer.pem
    OrganizationalUnitIdentifier: admin
  ordererOUIdentifier:
    Certificate: cacerts/localhost-1030-ca-orderer.pem
    OrganizationalUnitIdentifier: orderer' > "${PWD}/consortium/crypto-config/peerOrganizations/orderer/msp/config.yaml"
  echo
  echo "Registering peer0"
  echo 
  fabric-ca-client register --caname ca-orderer --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles "${PWD}/consortium/fabric-ca/orderer/tls-cert.pem"
  
  echo
  echo "Registering peer1"
  echo 
  fabric-ca-client register --caname ca-orderer --id.name peer1 --id.secret peer0pw --id.type peer --tls.certfiles "${PWD}/consortium/fabric-ca/orderer/tls-cert.pem"
  

  echo
  echo "Registering user"
  echo
  fabric-ca-client register --caname ca-orderer --id.name user1 --id.secret user1pw --id.type client --tls.certfiles "${PWD}/consortium/fabric-ca/orderer/tls-cert.pem"
  
  echo
  echo "Registering the org admin"
  echo
  fabric-ca-client register --caname ca-orderer --id.name ordereradmin --id.secret org1adminpw --id.type admin --tls.certfiles "${PWD}/consortium/fabric-ca/orderer/tls-cert.pem"
  
   mkdir -p /consortium/crypto-config/peerOrganizations/orderer/peers


  mkdir -p /consortium/crypto-config/peerOrganizations/orderer/peers/peer0.orderer 
  echo  "Generating the peer0 msp"
  echo
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:1030 --caname ca-orderer -M "${PWD}/consortium/crypto-config/peerOrganizations/orderer/peers/peer0.orderer/msp" --csr.hosts peer0.orderer --tls.certfiles "${PWD}/consortium/fabric-ca/orderer/tls-cert.pem"


  cp "${PWD}/consortium/crypto-config/peerOrganizations/orderer/msp/config.yaml" "${PWD}/consortium/crypto-config/peerOrganizations/orderer/peers/peer0.orderer/msp/config.yaml"

  echo
  echo "Generating the peer0-tls certificates"
  echo
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:1030 --caname ca-org1 -M "${PWD}/consortium/crypto-config/peerOrganizations/orderer/peers/peer0.orderer/tls" --enrollment.profile tls --csr.hosts peer0.orderer --csr.hosts localhost --tls.certfiles "${PWD}/consortium/fabric-ca/orderer/tls-cert.pem"
  

  cp "${PWD}/consortium/crypto-config/peerOrganizations/orderer/peers/peer0.orderer/tls/tlscacerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/orderer/peers/peer0.orderer/tls/ca.crt"
  cp "${PWD}/consortium/crypto-config/peerOrganizations/orderer/peers/peer0.orderer/tls/signcerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/orderer/peers/peer0.orderer/tls/server.crt"
  cp "${PWD}/consortium/crypto-config/peerOrganizations/orderer/peers/peer0.orderer/tls/keystore/"* "${PWD}/consortium/crypto-config/peerOrganizations/orderer/peers/peer0.orderer/tls/server.key"

  mkdir -p "${PWD}/consortium/crypto-config/peerOrganizations/orderer/msp/tlscacerts"
  cp "${PWD}/consortium/crypto-config/peerOrganizations/orderer/peers/peer0.orderer/tls/tlscacerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/orderer/msp/tlscacerts/ca.crt"

  mkdir -p "${PWD}/consortium/crypto-config/peerOrganizations/orderer/tlsca"
  cp "${PWD}/consortium/crypto-config/peerOrganizations/orderer/peers/peer0.orderer/tls/tlscacerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/orderer/peers/peer0.orderer/tlsca/tlsca.orderer-cert.pem"

  mkdir -p "${PWD}/consortium/crypto-config/peerOrganizations/orderer/ca"
  cp "${PWD}consortium/crypto-config/peerOrganizations/orderer/peers/peer0.orderer/msp/cacerts/"* "${PWD}/consortium/crypto-config/peerOrganizations/orderer/ca/ca.orderer-cert.pem"

  infoln "Generating the user msp"
  set -x
  fabric-ca-client enroll -u https://user1:user1pw@localhost:1030 --caname ca-org1 -M "${PWD}/consortium/crypto-config/peerOrganizations/orderer/users/User1@orderer/msp" --tls.certfiles "${PWD}/consortium/fabric-ca/orderer/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/consortium/crypto-config/peerOrganizations/orderer/msp/config.yaml" "${PWD}/consortium/crypto-config/peerOrganizations/orderer/users/User1@orderer/msp/config.yaml"
  echo
  echo "Generating the org admin msp"
  echo
  fabric-ca-client enroll -u https://org1admin:org1adminpw@localhost:1030 --caname ca-org1 -M "${PWD}/consortium/crypto-config/peerOrganizations/orderer/users/Admin@orderer/msp" --tls.certfiles "${PWD}/consortium/fabric-ca/orderer/tls-cert.pem"
 

  cp "${PWD}/consortium/crypto-config/peerOrganizations/orderer/msp/config.yaml" "${PWD}/consortium/crypto-config/peerOrganizations/orderer/users/Admin@orderer/msp/config.yaml"
}

# COMPOSE_FILE_CA=docker/docker-compose_ca.yaml
# IMAGE_TAG= docker-compose -f $COMPOSE_FILE_CA up -d 2>&1
# sleep 6
# docker ps 

infoln "creating Hospital Identities"
certificatesForHospital

infoln "creating Laboratory Identities"
certificatesForLaboratory

infoln "creating Pharamacy Identities"
certificatesForPharamacy

infoln "creating Ins-provider Identities"
certificatesForIns-provider

infoln "creating Orderer Identities"
certificatesForOrderer
